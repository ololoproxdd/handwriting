﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class Authentication
    {
        private readonly TimeSpan[] MORNING = {new TimeSpan(4, 0, 1), new TimeSpan(12, 0, 0) };
        private readonly TimeSpan[] DAY = { new TimeSpan(12, 0, 1), new TimeSpan(18, 0, 0) };
        private readonly TimeSpan[] EVENING = { new TimeSpan(18, 0, 1), new TimeSpan(4, 0, 0) };
        private readonly Func<List<double>, List<double>, bool>[,] filters;
        private enum dayZone { morning, day, evening };



        public Authentication()
        {
            // массив функций. Нужен стобы для определенного времени суток вызывать опредленный фильтр
            filters = new Func<List<double>, List<double>, bool>[3, 3]{
                {
                    (perfectTime, realTime) => AdditiveFilter(perfectTime, realTime),
                    (perfectTime, realTime) => MultiplicativeFilter(perfectTime, realTime),
                    (perfectTime, realTime) => AdditiveFilter(perfectTime, realTime)
                },
                {
                    (perfectTime, realTime) => MultiplicativeFilter(perfectTime, realTime),
                    (perfectTime, realTime) => MultiplicativeFilter(perfectTime, realTime),
                    (perfectTime, realTime) => AdditiveFilter(perfectTime, realTime)
                },
                {
                    (perfectTime, realTime) => AdditiveFilter(perfectTime, realTime),
                    (perfectTime, realTime) => MultiplicativeFilter(perfectTime, realTime),
                    (perfectTime, realTime) => AdditiveFilter(perfectTime, realTime)
                },
            };
        }


        // параменрты оценки. 
        //Пределы отклонения времени в адитивном фильтре 
        private double minAdditiveDifferent = 0.01;
        private double maxAdditiveDifferent = 60;
        //Пределы отклонения количества ошибок в адитивном фильтре 
        private double minAdditive = 0;
        private double maxAdditive = 25;
        //Пределы отклонения времени в мультипликативном фильтре 
        private double minMultiplicativeDifferent = -0.40;
        private double maxMultiplicativeDifferent = 0.80;
        //Пределы количества ошибок в мультипликативном фильтре 
        private double minMultiplicative = 0;
        private double maxMultiplicative = 25;


        /// <summary>
        /// Проверка биометрических данных
        /// </summary>
        /// <param name="keyPhrase"></param>
        /// <param name="keyDownTime"></param>
        /// <param name="keyUpTime"></param>
        /// <returns></returns>
        public bool Check(KeyPhrase keyPhrase, List<double> keyDownTime, List<double> keyUpTime)
        {
            // берем текущее время чтобы опредлить в дальнейшем способ фильтрации
            TimeSpan date = DateTime.Now.TimeOfDay;

            // применяем адитивный фильтр. Так как данные хранятся в строковом формате ввиде json используем JsonDecode
            // для получения массива double в котором записано эталонное время  
            if (!AdditiveFilter(JsonConverter.JsonDecode<List<double>>(keyPhrase.MathExpectationWithholding), keyDownTime))
            {
                return false;
            }

            // определяем какое время суток сейчас
            dayZone lerningDayZone = GetDayZone(keyPhrase.Time), todayDayZone = GetDayZone(date);

            // в зависимости от времени суток применяем либо адитивный либо мультиплекативный фильтр
            return filters[(int)lerningDayZone, (int)todayDayZone](JsonConverter.JsonDecode<List<double>>(keyPhrase.MathExpectationInterval), keyUpTime);
        }

        /// <summary>
        /// Время интервалов между наэатием клавиш
        /// </summary>
        /// <param name="keyPhrases"></param>
        /// <returns></returns>
        public List<double> TimeIntervals(List<KeyPhraseElement> keyPhrases)
        {
            var intervals = new List<double>();
            for (int i = 1; i < keyPhrases.Count; i++)
            {
                // считаем интервал между нажатиями
                intervals.Add((keyPhrases[i].TimeKeyDown - keyPhrases[i - 1].TimeKeyUp).TotalMilliseconds);
            }
            return intervals;
        }

        /// <summary>
        /// Время удержания кнопки
        /// </summary>
        /// <param name="keyPhrases"></param>
        /// <returns></returns>
        public List<double> TimeWithholding(List<KeyPhraseElement> keyPhrases)
        {
            var withholding = new List<double>();
            for (int i = 0; i < keyPhrases.Count; i++)
            {
                // считаем интервал удержания клавиши
                withholding.Add((keyPhrases[i].TimeKeyUp - keyPhrases[i].TimeKeyDown).TotalMilliseconds);
            }
            return withholding;
        }



        /*
         * Адитивный фильтр
         * Параметры: perfectTime - эталон, realTime - полученные при вводе пользователем
        */
        private bool AdditiveFilter(List<double> perfectTime, List<double> realTime)
        {
            int countElement = perfectTime.Count(), countError = 0;
            double different;

            // считаем количество ошибок, то есть количество символов, у которых разница с эталонной больше или меньше заданной
            for (int i = 0; i < countElement; i++)
            {
                different = Math.Abs(perfectTime[i] - realTime[i] / perfectTime[i] * 100);
                if (different > maxAdditiveDifferent || different < minAdditiveDifferent)
                {
                    countError++;
                }
            }

            //определяем процентное соотношение ошибок ко всем символам и сравниваем с заданным значением
            different = (double)countError / countElement * 100;
            if (different > maxAdditive || different < minAdditive)
            {
                return false;
            }

            return true;
        }

        /*
         * Мультипликативный фильтр 
         * Параметры: perfectTime - эталон, realTime - полученные при вводе пользователем
        */
        private bool MultiplicativeFilter(List<double> perfectTime, List<double> realTime)
        {
            int countElement = perfectTime.Count(), countError = 0;
            double different;

            // считаем количество ошибок, то есть количество символов, у которых разница с эталонной больше или меньше заданной
            for (int i = 0; i < countElement; i++)
            {
                different = Math.Abs(realTime[i] / perfectTime[i]);
                if (different > maxMultiplicativeDifferent || different < minMultiplicativeDifferent)
                {
                    countError++;
                }
            }

            //определяем процентное соотношение ошибок ко всем символам и сравниваем с заданным значением
            different = (double)countError / countElement * 100;
            if (different > maxMultiplicative || different < minMultiplicative)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Определяет какое сейчас время суток по переданной дате
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private dayZone GetDayZone(TimeSpan date)
        {
            if (date >= MORNING[0] & date <= MORNING[1])
            {
                return dayZone.morning;
            }
            if (date >= DAY[0] & date <= DAY[1])
            {
                return dayZone.day;
            }
            return dayZone.evening;
        }
    }
}

﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    /// <summary>
    /// Класс для добавления нового почерка
    /// </summary>
    public class Learning
    {
        /// <summary>
        /// Записываются все повторные вводы ключевой фразы
        /// </summary>
        public List<List<KeyPhraseElement>> listKeyPhraseElements { get; private set; }
        /// <summary>
        /// Ключевая фраза
        /// </summary>
        public KeyPhrase keyPhrase { get; set; }
        /// <summary>
        /// Сектора, нужны для проверки равномерности ключевой фразы
        /// </summary>
        private char[,] Sectors = { 
            { '1', '2', 'й', 'ц' },
            { 'ф', 'ы', 'я', 'ч'},
            { '3', '4', 'у', 'к' },
            { 'в', 'а', 'с', 'м' },
            { '5', '6', 'е', 'н' },
            { 'п', 'р', 'и', 'т' },
            { '7', '8', 'г', 'ш' },
            { 'о', 'л', 'ь', 'б' },
            { '9', '0', 'щ', 'з' },
            { 'д', 'ж', 'ю', ' ' },
            { 'х', 'ъ', 'э', 'ё' }
        };

        /// <summary>
        /// Конструктор
        /// </summary>
        public Learning()
        {
            listKeyPhraseElements = new List<List<KeyPhraseElement>>();
        }
        /// <summary>
        /// Поиск номера сектора (строки) для элемента el
        /// </summary>
        /// <param name="el"></param>
        /// <returns></returns>
        private int GetSector(char el)
        {
            // проходим по строкам
            for (int i = 0; i < Sectors.GetLength(0); i++)
                // проходим по столбцам
                for (int j = 0; j < Sectors.GetLength(1); j++)
                    if (Sectors[i, j] == el)
                        return i;
            return -1;
        }

        /// <summary>
        /// Инициализация словаря для будущей провреки равномерности
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, bool> InitDictionaryCheck()
        {
            var checkSector = new Dictionary<int, bool>();
            for (int i = 0; i < Sectors.GetLength(0); i++)
                checkSector.Add(i, false);
            return checkSector;
        }

        /// <summary>
        /// true - если все элементы словаря равны true иначе false
        /// </summary>
        /// <returns></returns>
        private bool GetDictionaryResult(Dictionary<int, bool> checkSector)
        {
            for (int i = 0; i < checkSector.Count; i++)
                if (checkSector[i] == false) //если хоть один элемент false, то false всё условие
                    return false;
            return true;
        }

        /// <summary>
        /// Создание ключевой фразы
        /// </summary>
        /// <returns></returns>
        public KeyPhrase CreateKeyPhrase(string keyPhraseText)
        {
            //проверка на равномерность ключевой фразы
            int previousSector = -1; // предыдущий сеткор
            var checkSector = InitDictionaryCheck(); //словарь 
            for (int i = 0; i < keyPhraseText.Length; i++)
            {
                int currentSector = GetSector(keyPhraseText[i]); // текущий сектор
                if (previousSector == currentSector) //то нарушается условие равномерности, соседние символы должны быть из разных секторов
                    throw new Exception("Ключевая фраза не равномерна");

                checkSector[currentSector] = true; //Отмечаем что элемент из этого сектора был в ключевой фразе
                previousSector = currentSector;
            }
            //то нарушается условие равномерности, в ключевой фразе должны быть символы из всех секторов
            if (GetDictionaryResult(checkSector) == false) 
                throw new Exception("Ключевая фраза не равномерна");
            keyPhrase =  new KeyPhrase
            {
                Text = keyPhraseText
            };
            return keyPhrase;
        }

        /// <summary>
        /// Подсчёт значений для занесения в БД
        /// </summary>
        /// <param></param>
        public void CalculateMathExpectations()
        {
            var withholding = new List<double>();
            var interval = new List<double>();
            int n = listKeyPhraseElements.Count, m = listKeyPhraseElements[0].Count;
            for (int i = 0; i < m; i++)
            {
                double MathExpectationWithholding = 0, MathExpectationInterval = 0;
                for (int j = 0; j < n; j++)
                {
                    //подсчтываем время удержания для каждого символа
                    MathExpectationWithholding += (listKeyPhraseElements[j][i].TimeKeyUp - listKeyPhraseElements[j][i].TimeKeyDown).TotalMilliseconds;
                    //подсчтыаем интервал между нажатием двух соседних символов
                    if (i > 0)
                        MathExpectationInterval += (listKeyPhraseElements[j][i].TimeKeyDown - listKeyPhraseElements[j][i - 1].TimeKeyUp).TotalMilliseconds;
                }

                // запись мат ожиданий для всех символов
                withholding.Add(MathExpectationWithholding / n);
                if (i > 0)
                    interval.Add(MathExpectationInterval / n);
            }

            //записываем мат ожидания
            keyPhrase.MathExpectationWithholding = JsonConverter.JsonEncode(withholding);
            keyPhrase.MathExpectationInterval = JsonConverter.JsonEncode(interval);
            //записываем время создания ключевой фразы
            keyPhrase.Time = DateTime.Now.TimeOfDay;
            //добавление ключевой фразы в бд
            using (var db = new Repository())
            {
                db.Add(keyPhrase);
            }
            //очистка листа
            listKeyPhraseElements.Clear();
        }

    }
}

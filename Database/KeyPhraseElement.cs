﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    /// <summary>
    /// Элемент ключевой фразы
    /// </summary>
    public class KeyPhraseElement
    {
        /// <summary>
        /// Код клавиши
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// Значение элемета
        /// </summary>
        public char Text { get; set; }
        /// <summary>
        /// Время нажатия клавиши
        /// </summary>
        public DateTime TimeKeyDown { get; set; }
        /// <summary>
        /// Время отпускания клавиши
        /// </summary>
        public DateTime TimeKeyUp { get; set; }
    }
}

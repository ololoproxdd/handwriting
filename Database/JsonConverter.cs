﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;

namespace Database
{
    /// <summary>
    /// Сериализация (перевод) в json формат
    /// </summary>
    public static class JsonConverter
    {
        /// <summary>
        /// Из формата T в JSON
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string JsonEncode<T>(T obj)
        {
            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(T));
            MemoryStream msObj = new MemoryStream();
            js.WriteObject(msObj, obj);
            msObj.Position = 0;
            StreamReader sr = new StreamReader(msObj);
 
            return sr.ReadToEnd();
        }

        /// <summary>
        /// Из формата JSON в формат T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T JsonDecode<T>(string str)
        {
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(str));
            DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(T));
            return (T)deserializer.ReadObject(ms);
        }
    }
}

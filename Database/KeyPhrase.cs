﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    /// <summary>
    /// Ключевая фраза
    /// </summary>
    public class KeyPhrase
    {
        public int Id { get; set; }
        public string Text { get; set; }
        /// <summary>
        /// Матиматические ожидания удержания кнопок (будет храниться json файл)
        /// </summary>
        public string MathExpectationWithholding { get; set; }
        /// <summary>
        /// Матиматические ожидания интервалов нажатия соседних символов (будет храниться json файл)
        /// </summary>
        public string MathExpectationInterval { get; set; }
        /// <summary>
        /// Время обучения
        /// </summary>
        public TimeSpan Time { get; set; }
    }
}

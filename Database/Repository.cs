﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    /// <summary>
    /// Класс работ с базой данных
    /// </summary>
    public class Repository : DbContext
    {
        /// <summary>
        /// Все ключевые фразы
        /// </summary>
        public DbSet<KeyPhrase> KeyPhrases { get; set; }
        public Repository() : base ("DBConnection")
        {
        }

        /// <summary>
        /// Добавление ключевой фразы
        /// </summary>
        /// <param name="keyPhrase"></param>
        public void Add(KeyPhrase keyPhrase)
        {
            KeyPhrases.Add(keyPhrase);
            SaveChanges();
        }

        /// <summary>
        /// Удаление ключевой фразы
        /// </summary>
        /// <param name="keyPhrase"></param>
        public void Remove(KeyPhrase keyPhrase)
        {
            KeyPhrases.Remove(keyPhrase);
            SaveChanges();
        }

        /// <summary>
        /// Получение ключевой фразы
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public KeyPhrase Get(string text)
        {
            return KeyPhrases.SingleOrDefault(x => x.Text == text);
        }
    }
}

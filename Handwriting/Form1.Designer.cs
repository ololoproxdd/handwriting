﻿namespace Handwriting
{
    partial class Handwriting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.keyPhrasesListBox = new System.Windows.Forms.ListBox();
            this.addKeyPhraseButton = new System.Windows.Forms.Button();
            this.authButton = new System.Windows.Forms.Button();
            this.LearningGroupBox = new System.Windows.Forms.GroupBox();
            this.clearKeyPhrasebutton = new System.Windows.Forms.Button();
            this.learningLabel = new System.Windows.Forms.Label();
            this.keyPhraseTextBox = new System.Windows.Forms.TextBox();
            this.authGroupBox = new System.Windows.Forms.GroupBox();
            this.clearAuthButton = new System.Windows.Forms.Button();
            this.authTextBox = new System.Windows.Forms.TextBox();
            this.KeyPhrasesGroupBox = new System.Windows.Forms.GroupBox();
            this.keyPhrasesListBoxDeleteButton = new System.Windows.Forms.Button();
            this.LearningGroupBox.SuspendLayout();
            this.authGroupBox.SuspendLayout();
            this.KeyPhrasesGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // keyPhrasesListBox
            // 
            this.keyPhrasesListBox.FormattingEnabled = true;
            this.keyPhrasesListBox.Location = new System.Drawing.Point(6, 19);
            this.keyPhrasesListBox.Name = "keyPhrasesListBox";
            this.keyPhrasesListBox.Size = new System.Drawing.Size(372, 69);
            this.keyPhrasesListBox.TabIndex = 1;
            // 
            // addKeyPhraseButton
            // 
            this.addKeyPhraseButton.Location = new System.Drawing.Point(6, 45);
            this.addKeyPhraseButton.Name = "addKeyPhraseButton";
            this.addKeyPhraseButton.Size = new System.Drawing.Size(176, 23);
            this.addKeyPhraseButton.TabIndex = 3;
            this.addKeyPhraseButton.Text = "Добавить ключевую фразу";
            this.addKeyPhraseButton.UseVisualStyleBackColor = true;
            this.addKeyPhraseButton.Click += new System.EventHandler(this.addKeyPhraseButton_Click);
            // 
            // authButton
            // 
            this.authButton.Location = new System.Drawing.Point(6, 45);
            this.authButton.Name = "authButton";
            this.authButton.Size = new System.Drawing.Size(176, 23);
            this.authButton.TabIndex = 4;
            this.authButton.Text = "Войти";
            this.authButton.UseVisualStyleBackColor = true;
            this.authButton.Click += new System.EventHandler(this.authButton_Click);
            // 
            // LearningGroupBox
            // 
            this.LearningGroupBox.Controls.Add(this.clearKeyPhrasebutton);
            this.LearningGroupBox.Controls.Add(this.learningLabel);
            this.LearningGroupBox.Controls.Add(this.keyPhraseTextBox);
            this.LearningGroupBox.Controls.Add(this.addKeyPhraseButton);
            this.LearningGroupBox.Location = new System.Drawing.Point(12, 142);
            this.LearningGroupBox.Name = "LearningGroupBox";
            this.LearningGroupBox.Size = new System.Drawing.Size(391, 78);
            this.LearningGroupBox.TabIndex = 5;
            this.LearningGroupBox.TabStop = false;
            this.LearningGroupBox.Text = "Обучение";
            // 
            // clearKeyPhrasebutton
            // 
            this.clearKeyPhrasebutton.Location = new System.Drawing.Point(188, 45);
            this.clearKeyPhrasebutton.Name = "clearKeyPhrasebutton";
            this.clearKeyPhrasebutton.Size = new System.Drawing.Size(196, 23);
            this.clearKeyPhrasebutton.TabIndex = 6;
            this.clearKeyPhrasebutton.Text = "Очистить строку";
            this.clearKeyPhrasebutton.UseVisualStyleBackColor = true;
            this.clearKeyPhrasebutton.Click += new System.EventHandler(this.clearKeyPhrasebutton_Click);
            // 
            // learningLabel
            // 
            this.learningLabel.AutoSize = true;
            this.learningLabel.Location = new System.Drawing.Point(6, 16);
            this.learningLabel.Name = "learningLabel";
            this.learningLabel.Size = new System.Drawing.Size(0, 13);
            this.learningLabel.TabIndex = 5;
            this.learningLabel.Visible = false;
            // 
            // keyPhraseTextBox
            // 
            this.keyPhraseTextBox.Location = new System.Drawing.Point(6, 19);
            this.keyPhraseTextBox.Name = "keyPhraseTextBox";
            this.keyPhraseTextBox.Size = new System.Drawing.Size(378, 20);
            this.keyPhraseTextBox.TabIndex = 4;
            this.keyPhraseTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyPhraseTextBox_KeyDown);
            this.keyPhraseTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.keyPhraseTextBox_KeyPress);
            this.keyPhraseTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyPhraseTextBox_KeyUp);
            // 
            // authGroupBox
            // 
            this.authGroupBox.Controls.Add(this.clearAuthButton);
            this.authGroupBox.Controls.Add(this.authTextBox);
            this.authGroupBox.Controls.Add(this.authButton);
            this.authGroupBox.Location = new System.Drawing.Point(12, 226);
            this.authGroupBox.Name = "authGroupBox";
            this.authGroupBox.Size = new System.Drawing.Size(391, 80);
            this.authGroupBox.TabIndex = 6;
            this.authGroupBox.TabStop = false;
            this.authGroupBox.Text = "Аутентификация";
            // 
            // clearAuthButton
            // 
            this.clearAuthButton.Location = new System.Drawing.Point(188, 45);
            this.clearAuthButton.Name = "clearAuthButton";
            this.clearAuthButton.Size = new System.Drawing.Size(196, 23);
            this.clearAuthButton.TabIndex = 7;
            this.clearAuthButton.Text = "Очистить строку";
            this.clearAuthButton.UseVisualStyleBackColor = true;
            this.clearAuthButton.Click += new System.EventHandler(this.clearAuthButton_Click);
            // 
            // authTextBox
            // 
            this.authTextBox.Location = new System.Drawing.Point(6, 19);
            this.authTextBox.Name = "authTextBox";
            this.authTextBox.Size = new System.Drawing.Size(378, 20);
            this.authTextBox.TabIndex = 0;
            this.authTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.authTextBox_KeyDown);
            this.authTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.authTextBox_KeyPress);
            this.authTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.authTextBox_KeyUp);
            // 
            // KeyPhrasesGroupBox
            // 
            this.KeyPhrasesGroupBox.Controls.Add(this.keyPhrasesListBoxDeleteButton);
            this.KeyPhrasesGroupBox.Controls.Add(this.keyPhrasesListBox);
            this.KeyPhrasesGroupBox.Location = new System.Drawing.Point(18, 12);
            this.KeyPhrasesGroupBox.Name = "KeyPhrasesGroupBox";
            this.KeyPhrasesGroupBox.Size = new System.Drawing.Size(385, 124);
            this.KeyPhrasesGroupBox.TabIndex = 7;
            this.KeyPhrasesGroupBox.TabStop = false;
            this.KeyPhrasesGroupBox.Text = "Список ключевых фраз";
            // 
            // keyPhrasesListBoxDeleteButton
            // 
            this.keyPhrasesListBoxDeleteButton.Location = new System.Drawing.Point(6, 95);
            this.keyPhrasesListBoxDeleteButton.Name = "keyPhrasesListBoxDeleteButton";
            this.keyPhrasesListBoxDeleteButton.Size = new System.Drawing.Size(176, 23);
            this.keyPhrasesListBoxDeleteButton.TabIndex = 4;
            this.keyPhrasesListBoxDeleteButton.Text = "Удалить выбранный элемент";
            this.keyPhrasesListBoxDeleteButton.UseVisualStyleBackColor = true;
            this.keyPhrasesListBoxDeleteButton.Click += new System.EventHandler(this.keyPhrasesListBoxDeleteButton_Click);
            // 
            // Handwriting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 310);
            this.Controls.Add(this.KeyPhrasesGroupBox);
            this.Controls.Add(this.authGroupBox);
            this.Controls.Add(this.LearningGroupBox);
            this.KeyPreview = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Handwriting";
            this.Text = "Аутентификация по почерку";
            this.Load += new System.EventHandler(this.Handwriting_Load);
            this.LearningGroupBox.ResumeLayout(false);
            this.LearningGroupBox.PerformLayout();
            this.authGroupBox.ResumeLayout(false);
            this.authGroupBox.PerformLayout();
            this.KeyPhrasesGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox keyPhrasesListBox;
        private System.Windows.Forms.Button addKeyPhraseButton;
        private System.Windows.Forms.Button authButton;
        private System.Windows.Forms.GroupBox LearningGroupBox;
        private System.Windows.Forms.TextBox keyPhraseTextBox;
        private System.Windows.Forms.GroupBox authGroupBox;
        private System.Windows.Forms.TextBox authTextBox;
        private System.Windows.Forms.GroupBox KeyPhrasesGroupBox;
        private System.Windows.Forms.Label learningLabel;
        private System.Windows.Forms.Button clearKeyPhrasebutton;
        private System.Windows.Forms.Button keyPhrasesListBoxDeleteButton;
        private System.Windows.Forms.Button clearAuthButton;
    }
}


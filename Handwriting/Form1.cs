﻿using Database;
using Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Handwriting
{
    public partial class Handwriting : Form
    {
        #region поля класса
        /// <summary>
        /// Класс аутентификации
        /// </summary>
        Authentication Authentication { get; set; }
        /// <summary>
        /// Список символов ключевой фразы с временами
        /// </summary>
        List<KeyPhraseElement> KeyPhraseElements { get; set; }

        /// <summary>
        /// Список символов ключевой фразы с временами для аутентификации
        /// </summary>
        List<KeyPhraseElement> CheckedKeyPhraseElements { get; set; }

        /// <summary>
        /// Для работы с добавлением ключевой фразы
        /// </summary>
        Learning Learning { get; set; }

        /// <summary>
        /// Счётчик повтора ключевой записи
        /// </summary>
        int RepeatIterator { get; set; }

        /// <summary>
        /// Максимальное количество повторов ключевой записи
        /// </summary>
        int RepeatCount { get; set; }

        #endregion

        #region конструктор
        public Handwriting()
        {
            InitializeComponent();
            Learning = new Learning();
            KeyPhraseElements = new List<KeyPhraseElement>();
            CheckedKeyPhraseElements = new List<KeyPhraseElement>();
            RepeatIterator = -1;
            RepeatCount = 5;
            Authentication = new Authentication();
        }
        #endregion

        #region добавление ключевой фразы
        /// <summary>
        /// Обработка нажатия кнопки "Добавить ключевую фразу"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addKeyPhraseButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Впервые добавляем ключевую фразу
                if (RepeatIterator == -1)
                {
                    Learning.CreateKeyPhrase(keyPhraseTextBox.Text);
                    RepeatIterator++;
                    AddLearningLabel();
                }
                //Делаем повтор ранее введённой ключевой фразы
                else
                {
                    //если неверно при повторе вводим ключевую фразу, то выводим ошибку
                    if (Learning.keyPhrase.Text != keyPhraseTextBox.Text)
                        throw new Exception("Неверно введена ключевая фраза при повторе");
                    RepeatIterator++;
                    Learning.listKeyPhraseElements.Add(new List<KeyPhraseElement>(KeyPhraseElements));
                    //очистка листа для дальнейшней работы
                    KeyPhraseElements.Clear();
                    // Когда ввели все повторные попытки, то добавляем в БД ключевую фразу
                    // с соответствующими данными
                    if (RepeatIterator >= RepeatCount)
                    {
                        MessageBox.Show("Ключевая фраза добавлена");
                        RepeatIterator = -1;
                        RemoveLearningLabel();
                        Learning.CalculateMathExpectations();
                        RenderingListBox();
                    }
                }
                learningLabel.Text = $"Повторите ключевую фразу '{Learning.keyPhrase.Text}' ещё {RepeatCount - RepeatIterator} раз(а)";
                keyPhraseTextBox.Text = string.Empty;

            }
            // Генерация ошибки
            catch(Exception exception)
            {
                KeyPhraseElements.Clear();
                Learning.listKeyPhraseElements.Clear();
                MessageBox.Show(exception.Message);
            }
        }

        /// <summary>
        /// Перерисовка формы для повтора ключевой фразы
        /// </summary>
        private void AddLearningLabel()
        { 
            addKeyPhraseButton.Text = "Проверить ключевую фразу";
            clearKeyPhrasebutton.Location = new Point(clearKeyPhrasebutton.Location.X, 62);
            addKeyPhraseButton.Location = new Point(addKeyPhraseButton.Location.X, 62);
            LearningGroupBox.Size = new Size(LearningGroupBox.Size.Width, 94);
            keyPhraseTextBox.Location = new Point(keyPhraseTextBox.Location.X, 36);
            authGroupBox.Location = new Point(authGroupBox.Location.X, 242);
            ClientSize = new Size(ClientSize.Width, 330);
            learningLabel.Visible = true;
        }

        /// <summary>
        /// Перерисовка формы для обычной работы
        /// </summary>
        private void RemoveLearningLabel()
        {
            learningLabel.Visible = false;
            addKeyPhraseButton.Text = "Добавить ключевую фразу";
            clearKeyPhrasebutton.Location = new Point(clearKeyPhrasebutton.Location.X, 45);
            addKeyPhraseButton.Location = new Point(addKeyPhraseButton.Location.X, 45);
            LearningGroupBox.Size = new Size(LearningGroupBox.Size.Width, 78);
            keyPhraseTextBox.Location = new Point(keyPhraseTextBox.Location.X, 19);
            authGroupBox.Location = new Point(authGroupBox.Location.X, 226);
            ClientSize = new Size(ClientSize.Width, 310);
        }

        /// <summary>
        /// Нажатие символа в текстбоксе ключевой фразы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void keyPhraseTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (RepeatIterator > -1)
            {
                // добавлям новый символ ключевой фразы
                KeyPhraseElements.Add(new KeyPhraseElement
                {
                    Code = e.KeyValue, // код клавиши
                    TimeKeyDown = DateTime.Now // записыаем время нажатия клавиши
                });
            }
        }

        /// <summary>
        /// зажатие символа в текстбоксе ключевой фразы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void keyPhraseTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char value = e.KeyChar;
            //ввод только русских символов и цифр
            if (!((value >= 'а' && value <= 'я') || (value >= '0' && value <= '9') || value == 'ё' || value == ' '))
            {
                if (RepeatIterator > -1)
                {
                    //Если символ не русская буква и цифра, то удаляем его из листа
                    //символов ключевой фразы
                    KeyPhraseElements.Remove(KeyPhraseElements.Last());
                }
                e.Handled = true;
                return;
            }
            if (RepeatIterator > -1)
            {
                // добавлям новый символ ключевой фразы, так как в keyPhraseTextBox_KeyDown
                // его невозможно получить (там только англ символы можно получить)
                KeyPhraseElements.Last().Text = value;
            }
        }

        /// <summary>
        /// Отпускание клавиши при вводе ключевой фразы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void keyPhraseTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            KeyPhraseElement element;
            if (RepeatIterator > -1 && (element = KeyPhraseElements.LastOrDefault(x => x.Code == e.KeyValue)) != null)
            {
                if (element.Text == 0)
                {
                    KeyPhraseElements.Remove(element);
                }
                else
                {
                    element.TimeKeyUp = DateTime.Now; // записываем время отпуская клавиши
                }
            }
        }

        /// <summary>
        /// Очищаем текстбокс ключевой фразы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clearKeyPhrasebutton_Click(object sender, EventArgs e)
        {
            keyPhraseTextBox.Clear();
            KeyPhraseElements.Clear();
        }
        #endregion

        #region аутентификация
        /// <summary>
        /// Нажатие символа в текстбоксе аутентификации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            CheckedKeyPhraseElements.Add(new KeyPhraseElement
            {
                Code = e.KeyValue,
                TimeKeyDown = DateTime.Now
            });
        }

        /// <summary>
        /// Зажатие символа в текстбоксе аутентификации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char value = e.KeyChar;
            if (!((value >= 'а' && value <= 'я') || (value >= '0' && value <= '9') || value == 'ё' || value == ' '))
            {
                CheckedKeyPhraseElements.Remove(CheckedKeyPhraseElements.Last());
                e.Handled = true;
                return;
            }
            CheckedKeyPhraseElements.Last().Text = e.KeyChar;
        }

        /// <summary>
        /// Отпускание символа в текстбоксе аутентификации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            KeyPhraseElement element;
            if ((element = CheckedKeyPhraseElements.LastOrDefault(x => x.Code == e.KeyValue)) != null)
            {
                if (element.Text == 0)
                {
                    CheckedKeyPhraseElements.Remove(element);
                }
                else
                {
                    element.TimeKeyUp = DateTime.Now;
                }
            }
        }

        /// <summary>
        /// Очистка текстбокса аутентификации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clearAuthButton_Click(object sender, EventArgs e)
        {
            authTextBox.Clear();
            CheckedKeyPhraseElements.Clear();
        }

        /// <summary>
        /// Нажатие кнопки аутентификации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void authButton_Click(object sender, EventArgs e)
        {
            // определяем интервалы между нажатиями
            List<double> keyUpTime = Authentication.TimeIntervals(CheckedKeyPhraseElements);
            // определяем интервалы удержания
            List<double> keyDownTime = Authentication.TimeWithholding (CheckedKeyPhraseElements);
            KeyPhrase keyPhrase;
            using (var db = new Repository())
            {
                // достаем соответсвующую последовательность из бд
                keyPhrase = db.Get(authTextBox.Text);
            }
            if (keyPhrase != null)
            {
                // проверяем эту последовательность
                if (Authentication.Check(keyPhrase, keyDownTime, keyUpTime))
                {
                    MessageBox.Show("Верно");
                }
                else
                {
                    MessageBox.Show("Не верно");
                }
            } else
            {
                MessageBox.Show("Нет фраз");
            }
            CheckedKeyPhraseElements.Clear();
            authTextBox.Clear();
        }
        #endregion

        #region вся форма
        /// <summary>
        /// Загрузка всей формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Handwriting_Load(object sender, EventArgs e)
        {
            RenderingListBox();
        }
        #endregion

        #region листбокс с ключевыми фразами
        /// <summary>
        /// Удаление элемента из листбокса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void keyPhrasesListBoxDeleteButton_Click(object sender, EventArgs e)
        {
            //удаление из БД
            using (var db = new Repository())
            {
                var keyPhrase = db.KeyPhrases.Where(w => w.Text == keyPhrasesListBox.SelectedItem.ToString());
                if (keyPhrase.Count() > 0)
                    db.Remove(keyPhrase.First());
            }
            RenderingListBox();
        }

        /// <summary>
        /// Отображение на листбоксе ключевых фраз
        /// </summary>
        private void RenderingListBox()
        {
            using (var db = new Repository())
            {
                // загружаем данные из БД. Отображаем в листбоксе только тексты ключевых фраз
                keyPhrasesListBox.DataSource = db.KeyPhrases.Select(w => w.Text).ToArray();
            }
        }
        #endregion
    }
}
